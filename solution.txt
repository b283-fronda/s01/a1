// MySQL S01 Activity


//1. List the books Authored by Marjorie Green.
//Answer: The Busy Executive's Database Guide, You Can Combat Computer Stress!

//2. List the books Authored by Michael O'Leary.
//Answer: Cooking with Computers, TC7777

//3. Write the author/s of "The Busy Executive’s Database Guide".
//Answer: Marjorie Green, Abraham Bennet

//4. Identify the publisher of "But Is It User Friendly?".
//Answer: Algodata Infosystems

//5. List the books published by Algodata Infosystems.
//Answer: But Is It User Friendly?, The Busy Executive's Database Guide